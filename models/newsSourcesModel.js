const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const category = require('../models/categoriesModel');
const user = require('../models/usersModel');

const NewsSourceSchema = new Schema({
  id: { type: Int32Array },
  url: { type: URL },
  name: { type: String },
  category_id: category.schema.id,
  user_id: user.schema.id
});

const NewsSourceModel = mongoose.model('NewsSources', NewsSourceSchema);

module.exports = {
  model: NewsSourceModel,
  schema: NewsSourceSchema
}