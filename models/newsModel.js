const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const news_source = require('../models/newsSourcesModel');
const user = require('../models/usersModel');
const category = require('../models/categoriesModel');

const NewsSchema = new Schema({
  id: { type: Int32Array },
  title: { type: URL },
  short_description: { type: String },
  permanlink: { type: URL },
  date: { type: Date },
  news_source_id: news_source.schema.id,
  user_id: user.schema.id,
  category_id: category.schema.id
});

const NewsModel = mongoose.model('news', NewsSchema);

module.exports = {
  model: NewsModel,
  schema: NewsSchema
}