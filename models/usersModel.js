const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const role = require('../models/rolesModel');

const UserSchema = new Schema({
  id: { type: Int32Array },
  email: { type: String },
  first_name: { type: String },
  last_name: { type: String },
  role_id: role.schema.id
});

const UserModel = mongoose.model('users', UserSchema);

module.exports = {
  model: UserModel,
  schema: UserSchema
}